Parsing annotation
==================

**Use MEGAN tools to parse the outputs**::

	source conf_cluster.conf

"Meganize" the blast output::

$wd/deps/megan/tools/blast2rma --format BlastTab -i $GENE_PRED/diamond.*.$PRJ_NAME.m8 -a2t $DB/prot_acc2tax-Mar2018X1.abin -a2seed $DB/acc2seed-May2015XX.abin -o $GENE_PRED/temp_tax_seed.rma


Extract taxonomy annotation from the rma file::

$wd/megan/tools/rma2info --in $GENE_PRED/temp_tax_seed.rma -u false -v -l -m -r2c Taxonomy -mro -bo --out $GENE_PRED/id_gene2tax.$PRJ_NAME.tab > $GENE_PRED/id_gene2tax.$PRJ_NAME.tab

Extract functional information from the rma file::

$wd/megan/tools/rma2info -i $GENE_PRED/temp_tax_seed.rma -u false -r2c SEED -p true -v > $GENE_PRED/id_gene2seed.$PRJ_NAME.tab



Some file arrangments::

	sed -i 's/;/\t/g' $GENE_PRED/id_gene2tax.$PRJ_NAME.tab
	sed -i '/^#/ d' $GENE_PRED/id_gene2tax.$PRJ_NAME.tab

	sed -i 's/;/\t/g' $GENE_PRED/id_gene2seed.$PRJ_NAME.tab
	sed -i 's/\tSEED//g' $GENE_PRED/id_gene2seed.$PRJ_NAME.tab

	sort -k2 $GENE_PRED/id_gene2seed.$PRJ_NAME.tab > $GENE_PRED/id_gene2seed_sorted.$PRJ_NAME.tab
	python seed_processing.py $GENE_PRED/id_gene2seed_sorted.$PRJ_NAME.tab $DB/subsys_space.txt $GENE_PRED/id_gene2seed_sorted_path.$PRJ_NAME.tab

	sed -i 's/\t/;/g' $GENE_PRED/id_gene2seed_sorted_path.$PRJ_NAME.tab

**Merge all the outputs in a single table**

Prepare python environment::

	module load autoload python/3.5.2
	source $wd/env/bin/activate
	pip3 install pandas

Create output directory::

	mkdir -p $OUT

Move to coverage folder and create variables for next steps::

	cd $wd/$PRJ_NAME/5_Coverages/
	
	for i in $SAMPLE_LIST ; do 
		echo $i; 
		A="$COV/coverage.$i" ; 
		SAMPLES_COV="$A $SAMPLES_COV"; 
	done

Run the "join.py" script to merge the output::

	python $wd/join.py $GENE_PRED/id_gene2tax.$PRJ_NAME.tab $GENE_PRED/id_gene2seed_sorted.$PRJ_NAME.tab $DB/rev.subsys.txt $SAMPLES_COV $OUT/join_gene_id_seed.tab










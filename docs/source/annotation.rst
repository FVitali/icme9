Annotation
==========

**Use Diamond to annotate the predicted genes**

Source the configuration file::

	source conf_cluster.conf

Run Blast alignment using Diamond::

	diamond blastp --query $GENE_PRED/prodigal.idba.$PRJ_NAME.faa \
	--db $DB/nr \
	-o $GENE_PRED/diamond.prodigal.idba.$PRJ_NAME.m8 \
	--sensitive

**Alternatively, prepare a script for a batch submission.**

Create a new script file::
	
	nano Diamond.sh
	
Copy and paste the following test into the file Diamond.sh::
	
	#!/bin/bash

	cd $SLURM_SUBMIT_DIR
	source conf_cluster.conf

	echo "`date`: Diamond blast starts"

	diamond blastp --query $GENE_PRED/prodigal.idba.$PRJ_NAME.faa \
	--db $DB/nr \
	-o $GENE_PRED/diamond.prodigal.idba.$PRJ_NAME.m8 \
	--sensitive -p 12

	echo "`date`: Diamond blast ends"

and make it executable::

	chmod a+x Diamond.sh

Exit from inteactive job::

	exit	

and submit the script::

	sbatch -p gll_usr_prod --account train_embo2018 --reservation=s_tra_embo2018 --time 24:00:00 --mem=28000 -N 1 -n 12 ./Diamond.sh

Have a look to the submitted job::
	
	squeue -u <username>


Have a look to the results::

	cd $GENE_PRED
	ll
	less diamond.prodigal.idba.$PRJ_NAME.m8

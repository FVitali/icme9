Data analysis
==================

**Create table and graphical outputs**::

	source conf_cluster.conf
	cd $OUT
	
	module load autoload python/3.6.4
	source $wd/env/bin/activate

	sed -i 's/ /_/g' join_gene_id_seed.tab.pivot
	
Create a taxonomy Krona graph of all the community::

	cd $OUT	

	t=6
	for i in $SAMPLE_LIST; do
		ktImportTaxonomy -o $i.all.taxonomy.krona.html -t 1 -m $t join_gene_id_seed.tab.pivot
		t=$((t + 1))
	done

Create a SEDD Krona graph of all the community::

	s=6
	for i in $SAMPLE_LIST; do
		paste <(cut -f$s join_gene_id_seed.tab.pivot) <(cut -f2,3,4,5 join_gene_id_seed.tab.pivot) > $i.all.seed.tab
		ktImportText $i.all.seed.tab -o $i.all.seed.html
		s=$((s + 1))
	done


Create sub-table based on a SEED category::

	python $wd/mining.py join_gene_id_seed.tab.pivot lev2 "Resistance_to_antibiotics_and_toxic_compounds"
	
Create taxonomy Krona graphs based on the sub-table::

	m=6
	for i in $SAMPLE_LIST; do

		ktImportTaxonomy -o $i.Resistance_to_antibiotics_and_toxic_compounds.taxonomy.krona.html -t 1 -m $m Resistance_to_antibiotics_and_toxic_compounds.seed_tax.tab
		m=$((m + 1))

	done



#!/bin/bash

cd $SLURM_SUBMIT_DIR

source $CONF

echo "Importing modules"

module load profile/advanced

list=""
for i in $SAMPLE_LIST ;do list="${list}coverage.$i,";echo $string ;done
#erases last coma
list=${list%?}

#print list

echo -e "Importing python/3.5.2 module"

module load autoload python/3.5.2
source $wd/env/bin/activate

mkdir -p $OUT

cd $COV

for i in $SAMPLE_LIST ; do 
    echo $i; 
    NAME="$COV/coverage.$i" 
    SAMPLES_COV="$NAME $SAMPLES_COV"
    #echo "--->$NAME $SAMPLES_COV<---"
done

python $wd/join.py $GENE_PRED/id_gene2tax.$PRJ_NAME.tab $GENE_PRED/id_gene2seed_sorted.$PRJ_NAME.tab $DB/rev.subsys.txt $SAMPLES_COV $OUT/join_gene_id_seed.tab

sed -i "s/.0\t/\t/g" $OUT/join_gene_id_seed.tab

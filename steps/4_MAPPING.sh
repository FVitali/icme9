#!/bin/bash
cd $SLURM_SUBMIT_DIR

echo $CONF
source $CONF

echo "Performing Mapping"

echo "Create output folder: Mapping"
mkdir -p $MAPP

echo "Mapping reads against genes.."

echo "`date`: Building database"

echo "Inporting Bowtie"

PRED_INPUT=$(ls $GENE_PRED/ | grep fasta)

$LNBIN/bowtie2_bin/bowtie2-build $GENE_PRED/$PRED_INPUT $MAPP/bwt.idba.$PRJ_NAME

echo "`date`: Starting Bowtie2"

for i in $SAMPLE_LIST; do
    
    $LNBIN/bowtie2_bin/bowtie2 -x $MAPP/bwt.idba.$PRJ_NAME \
	    -1 $FILT_READS/filtered.$i.R1.fastq \
	    -2 $FILT_READS/filtered.$i.R2.fastq \
	    -U $FILT_READS/filtered.single.$i.fastq \
	    -S $MAPP/bwt.idba.$i.sam

done

echo "`date`: End of Bowtie2"


echo "-------------------------------------------------------------------------"

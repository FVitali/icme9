#!/bin/bash

cat<<EOF
    __  ___     __        ____  _                      ___ ___
   /  |/  /__  / /_____ _/ __ \(_)___  ___     _   __ <  /<  /
  / /|_/ / _ \/ __/ __ `/ /_/ / / __ \/ _ \   | | / / / / / / 
 / /  / /  __/ /_/ /_/ / ____/ / /_/ /  __/   | |/ / / / / /  
/_/  /_/\___/\__/\__,_/_/   /_/ .___/\___/    |___(_)_(_)_/   
                             /_/                               
EOF

printf "\n \033[0;31m THIS IS THE PICO VERSION!\033[0m \n"

#==== ARGUMENT CHECK ====
if [ $# -ne 1 ];then
    echo -e "The only argument should be the configuration file \n"
    echo -e "Usage: metagenomic_pipeline_1.1.sh <Confifuration File> \n"
    exit 1
fi

if [ -e $1 ]; then
    echo -e "Sourcing $1 \n"
    source $1
    echo -e "Configuration file is: $1 \n"
else
    echo "Configuratione file: $1 does not exists!"
    echo -e "Usage: metagenomic_pipeline_1.1.sh <Confifuration File> \n"
    echo "Quit"
    exit 1
fi

#if the assembler is spades, i need odd kmer length
if [ $ASSEMBLER -e 2 ]; then
    IFS=',' read -ra SP_K_VAL <<<"$SPADES_KMER"
    counter=0
    for i in ${SP_K_VAL[*]} ; do
	if [ $((i%2)) -eq 0 ];then
	    echo -e "ERROR"
	    echo -e "$i is a even number: Spades don't accept even numbers."
	    echo -e "Please change the value of kmer length"
	    exit 1
	fi
    done
fi
#==== Variables sourced from .conf====
echo -e "==============\n"
echo -e "Input folder"
echo $INPUT_DIR
echo -e "==============\n"

echo -e "\nProject name and sample list\n"
echo $PRJ_NAME
echo $SAMPLE_LIST
echo -e "==============\n"

echo -e "Average quality and length\n"
echo $AV_READS_Q
echo $AV_READS_L
echo -e "==============\n"

echo -e "Coassembly parameters\n"
echo $MIN_KMER_LEN
echo $MAX_KMER_LEN
echo $STEP
echo $SPADES_KMER

#==== PIPELINE START ====

if [ $FILTERING -ne 0 ];then
    ONE=$(qsub -A cin_staff -N Filtering -l walltime=24:00:00,select=1:ncpus=1:mem=6Gb -v CONF=$1 steps/1_FILTERING.sh )
    
    echo $ONE
fi

if [ $ASSEMBLY -ne 0 ];then
    if [ $FILTERING -ne 0 ];then
	TWO=$(qsub -W depend=afterok:$ONE -A cin_staff -N Assembly -l walltime=24:00:00,select=1:ncpus=36:mem=120Gb -v CONF=$1 steps/2_ASSEMBLY.sh)
    else
	TWO=$(qsub -A cin_staff -N Assembly -l walltime=24:00:00,select=1:ncpus=20:mem=120Gb -v CONF=$1 steps/2_ASSEMBLY.sh)
    fi
    echo $TWO
fi

if [ $GENE_PREDICTION -ne 0 ];then
    if [ $ASSEMBLY -ne 0 ];then
	THREE=$(qsub -W depend=afterok:$TWO -A cin_staff -N Gene_Pred -l walltime=24:00:00,select=1:ncpus=36:mem=120Gb -v CONF=$1 steps/3_GENE_PREDICTION.sh)
    else
	THREE=$(qsub -A cin_staff -N Gene_Pred -l walltime=24:00:00,select=1:ncpus=36:mem=120Gb -v CONF=$1 steps/3_GENE_PREDICTION.sh)
    fi
    echo $THREE
fi

if [ $MAPPING -ne 0 ];then
    if [ $GENE_PREDICTION -ne 0 ];then
	FOUR=$(qsub -W depend=afterok:$THREE -A cin_staff -N Mapping -l walltime=24:00:00,select=1:ncpus=4:mem=12Gb -v CONF=$1 steps/4_MAPPING.sh)
    else
	FOUR=$(qsub -A cin_staff -N Mapping -l walltime=24:00:00,select=1:ncpus=1:mem=6Gb -v CONF=$1 steps/4_MAPPING.sh)
    fi
    echo $FOUR
fi

if [ $COVERAGE -ne 0 ];then
    if [ $MAPPING -ne 0 ];then
	FIVE=$(qsub -W depend=afterok:$FOUR -A cin_staff -N Coverage -l walltime=24:00:00,select=1:ncpus=1:mem=6Gb -v CONF=$1 steps/5_COVERAGE.sh)
    else
	FIVE=$(qsub -A cin_staff -N Coverage -l walltime=24:00:00,select=1:ncpus=1:mem=6Gb -v CONF=$1 steps/5_COVERAGE.sh)
    fi
    echo $FIVE
fi

if [ $PARSING -ne 0 ];then
    if [ $COVERAGE -ne 0 ];then
	SIX=$(qsub -W depend=afterok:$SIX -A cin_staff -N Parsing -l walltime=02:00:00,select=1:ncpus=1:mem=6Gb -v CONF=$1 steps/7_OUTPUT_WRITE.sh)
    else
	SIX=$(qsub -A cin_staff -N Parsing -l walltime=02:00:00,select=1:ncpus=1:mem=6Gb -v CONF=$1 steps/7_OUTPUT_WRITE.sh)
    fi
    echo $SEVEN
fi

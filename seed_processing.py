"""
This program trimms the results from rma2info --in <yourfile> -n
true -r2c SEED.  It takes the names of the sequence in witch an enzyme
is found and add the path from a mapping file (subsys.txt)

The input files must be added as positional arguments: the first is
the SEED file the second the PATH file

USAGE:

python seed_processing.py <seed_file> <subsys_file>

"""
import sys
if len(sys.argv)==4:
    SEED = sys.argv[1]
    PATH = sys.argv[2]
    OUT_FILE = sys.argv[3]
else:
    print (__doc__)
    exit(1)

#open the files
SEED_FILE=open(SEED,"r")
PATH_FILE=open(PATH,"r")

#Read the content of the subsys_space.txt
subsys=PATH_FILE.read()
PATH_FILE.close()

#get all SEED level 1 entry
results=[]
SEED_1=[]
for line in subsys.split("\n"):
    try:
        SEED1_entry=line.split("\t")[0]
        if SEED1_entry=="":
            continue
        else:
            SEED_1.append(SEED1_entry)            
    except:
        print("SEED 1 not found")

indices=[]
POOL=[]

for line in SEED_FILE:
    SEQ_NAME=line.split("\t")[0]
    QUERY_SEED1=line.split("\t")[1]
    
    subsys_litte=[]
    #extract only the lines with the SEED level 1 present in each
    #entry of SEED_FILE

    if QUERY_SEED1 in SEED_1:
        #print(SEED_1)
        idx = [j for j, x in enumerate(SEED_1) if x == QUERY_SEED1]
        subsys_little = [subsys.split("\n")[index] for index in idx]

    else:
        print("ATTENTION: no level 1 from SEED has been found.")
        subsys_little=subsys
        continue

    #print(len(subsys_little))
    SEED_2=[]
    SEED_3=[]
    #extract only seed values for level 2 or 3 (only 3 levels present
    #in megan output)
    for element in subsys_little:
        try:
            SEED2_entry=element.split("\t")[1]
            
            SEED_2.append(SEED2_entry)            
        except:
            print("SEED 2 not found")
            continue
            
        try:
            SEED3_entry=element.split("\t")[2]
            
            SEED_3.append(SEED3_entry)            
        except:
            print("SEED 3 not found")
            continue

    QUERY_SEED2=line.split("\t")[2]
    
    #find entry present in SEED_FILE
    if QUERY_SEED2 == "":
        print("SEED_2 IS EMPTY")
        break
    else:
        if QUERY_SEED2 in SEED_2:
            idx = [j for j, x in enumerate(SEED_2) if x == QUERY_SEED2]
            
            subsys_little = [subsys_little.split("\n")[index] for index in idx]
            # #print(subsys_little)
        elif QUERY_SEED2 in SEED_3:
        
            idx = [j for j, x in enumerate(SEED_3) if x == QUERY_SEED2]
            try:
                 subsys_little = [subsys_little[index] for index in idx]
            except:
                print("NOPE")
            #print(subsys_little)
        else:
            print("ATTENTION: no level 2 from SEED has been found.")
            continue

    SEED_4=[]
    QUERY_SEED3=line.split("\t")[3]
    for element in subsys_little:
        try:
            SEED4_entry=element.split("\t")[3]
            SEED_4.append(SEED4_entry)            
        except:
            print("SEED 4 not found")
            continue
        
            
    if QUERY_SEED3 in SEED_4:
        idx = [j for j, x in enumerate(SEED_4) if x == QUERY_SEED3]
        
        subsys_little = [subsys_little[index] for index in idx]            

    results_entry=SEQ_NAME+"\t"+str(subsys_little[0])+"\n"
    results.append(results_entry)
    print(results_entry)
    
FINAL=open(OUT_FILE,"w",500000)

for entry in results:
    FINAL.write(entry)

FINAL.close
SEED_FILE.close

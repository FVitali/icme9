import pandas as pd
import numpy as np
import re
import sys
import io
import argparse
import datetime

#---- FUNCIONS DEFINITION ----

def FilterByTaxId(joined,TaxId):
    """This function filters the joined data_table from join.py and
    retains only the wanted ID"""

    #to filter the dataset with the tax_id, numbers should be converted in str
    joined.tax_id=joined.tax_id.apply(str)

    A=joined[joined["tax_id"]==(str(TaxId))]

    #    A=joined["tax_id"]==(TaxId)
               #,flags=re.fullmatch))
    
    #joined=joined.dropna(subset=[TaxId])
    #actuall filtering
    #NB: "~" is the boolean operator for not
    #B=joined.drop(joined[~A].index.values)
    return(A)

# def FilterByTaxPath(joined,TaxName):
#     """This function filters the joined data_table from join.py and
#     retains only the wanted Taxon

#     NB: the groups are filtered with str.contains, same as bash 'grep'
#     no taxonomic rank is chosed
#     """
#     #joined=joined.dropna(subset=[TaxName])
#     A=joined['tax_path'].str.contains(TaxName)
    
#     #actuall filtering
#     #NB: "~" is the boolean operator for not
#     B=joined.drop(joined[~A].index.values)
#     return(B)


def FilterByFunction(joined,SeedLevel,Function):
    """This function allows to select a function.
    Arguments:
    1) the file containig the data
    2) the SEED level (1-4) one wants to analyze
    3) The Name of the funcion

    NB: all NaN rows in the interested SEED_level are discarded

    """
    #No Nan to be considered
    
    joined=joined.dropna(subset=[SeedLevel])
    
    A=joined[SeedLevel].str.contains(Function)
    B=joined.drop(joined[~A].index.values)
    return(B)

#---- END ----

#---- EXECUTION ----

#For clarity and easiness, flags and positiona parameters are defined
parser = argparse.ArgumentParser(description = "Subsets the joined script to extract some functions, taxa or boh ") 

parser.add_argument("subset", 
                    help = """What must be used to subset the file: ID = tax id, Name = tax name,
                    Function = Seed description.  NB: THE FUNCTION
                    NAME MUST BE QUOTED!  """,
                    choices = ["tax_id","function"]
)

parser.add_argument("--second_filter",
                    help = """ If present, it further subset the dataframe obtained by the 'subset' argument: 
                    Eg: python subset.py tax_id --function chemiotaxis allows select only the genes codifing
                    for the seed funcion chemiotaxis inside the subset found in tax_id""",
                    choices = ["tax_id","function"]
)


parser.add_argument("--tax_id",
                    help = "Taxon code (NCBI) you are interested in"
)

parser.add_argument("--tax_name",
                    help = "Taxon name you are interested in"
)

parser.add_argument("--seed",
                    help = "The seed function you want to analyze"
)

parser.add_argument("--seed_level",
                    help = "The seed level you want to analyze"
)

parser.add_argument("--inp",
                    help = "Input file"
)


args = parser.parse_args()


# Function run
#import files

joined=pd.read_csv(args.inp,sep="\t")

if args.subset == "tax_id":
    Out_Name=args.tax_id
    filtered = FilterByTaxId(joined,args.tax_id)
    if args.second_filter:
        real_seed_level="SEED_Level"+args.seed_level
        filtered = FilterByFunction(filtered,real_seed_level,args.seed)
        Out_Name=Out_Name+"_"+args.seed.replace(" ","_")
        
    print(filtered)
    now = datetime.datetime.now()
    Hash=str(now.year)+str(now.month)+str(now.day)+str(now.hour)+str(now.minute)+str(now.second)
    Out_Name=Out_Name+"_"+Hash+".csv"

    filtered = filtered.replace(np.nan,'Unknown',regex=True)
    print(filtered)
    filtered.to_csv(path_or_buf=Out_Name,index=False)

# elif args.subset == "tax_name":
#     #first part of output fine name
#     Out_Name=args.tax_name.replace(" ","_")

#     filtered = FilterByTaxPath(joined,args.tax_name)

#     if args.second_filter:
#         real_seed_level="SEED_Level"+args.seed_level
#         filtered = FilterByFunction(filtered,real_seed_level,args.seed)
#         Out_Name=Out_Name+"_"+args.seed.replace(" ","_")

#     print(filtered)
#     now = datetime.datetime.now()
#     Hash=str(now.year)+str(now.month)+str(now.day)+str(now.hour)+str(now.minute)+str(now.second)
#     Out_Name=Out_Name+"_"+Hash+".csv"
#     filtered = filtered.replace(np.nan,'Unknown',regex=True)
#     print(filtered)
#     filtered.to_csv(path_or_buf=Out_Name,index=False)


elif args.subset == "function":
    Out_Name = args.seed.replace(" ","_")
    real_seed_level="SEED_Level"+args.seed_level
    filtered = FilterByFunction(joined,real_seed_level,args.seed)
    if args.second_filter == "tax_id":
        filtered = FilterByTaxId(filtered,args.tax_id)
        Out_Name=Out_Name+"_"+args.tax_id.replace(" ","_")
    elif args.second_filter == "tax_name":
        filtered = FilterByTaxId(filtered,args.tax_name)
        Out_Name=Out_Name+"_"+args.tax_name.replace(" ","_")

    filtered = filtered.replace(np.nan,'Unknown',regex=True)
    print(filtered)
    filtered.to_csv()
